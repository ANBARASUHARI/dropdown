from django.shortcuts import render
from .forms import UserForm
from .models import User_dd
from django.shortcuts import render, redirect


# Create your views here.

def dropdownlist(request):
    print(request.method)
    if request.method == 'POST':
        form = UserForm(request.POST)
        if form.is_valid():
            first_name = request.POST["first_name"]
            father_name = request.POST["father_name"]
            age = request.POST["age"]
            gender = request.POST["gender"]

            user = User_dd(first_name=first_name, father_name=father_name, age=age, gender=gender)
            user.save()
            #user = form.save() # This will save the user to the database

        return redirect('/')
    else:
        form = UserForm()
        data = {"form": form}
    return render(request, 'dropdownlist.html', context=data)
