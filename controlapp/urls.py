from django.urls import path
from . import views

urlpatterns = [
    path("", views.dropdownlist, name="dropdownlist"),
]
