from django import forms
from django.db import models


gender_cate = [('male', 'Male'), ('female', 'Female'), ('others', 'Others')]


class UserForm(forms.Form):

    first_name = forms.CharField(max_length=100)
    father_name = forms.CharField(max_length=100)
    age = forms.IntegerField()
    #gender = forms.ChoiceField(choices=[('male', 'Male'), ('female', 'Female'), ('others', 'Others')])
    gender = forms.CharField(label="Gender", widget=forms.Select(choices=gender_cate))

    #class Meta:
    #   db_table = "user_table"